# -*- coding: utf-8 -*-
"""
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.

"""


import base64


def STR2BYTES(x, encoding='latin1'):
    return x.encode(encoding)


def f2b64(archivofuente):
    f = open(archivofuente, 'rb') #read binay (mode)
    data = f.read()
    f.close()
    b64data=base64.b64encode(data)
    cadenaprimitiva=str(b64data)
    archivo64=archivofuente+'.v64'
    text_file=open(archivo64,'w')
    text_file.write(str(cadenaprimitiva))
    text_file.close()
    
def b642f(archivo64):
    f=open(archivo64,'r')
    cadenaprimitiva0=f.read()
    f.close()
    long=len(cadenaprimitiva0)
    cadenaprimitiva=''
    for cont in range (long):
        if (cont!=0 and cont!=1 and cont!=(long-1)):
            cadenaprimitiva+=cadenaprimitiva0[cont]    
    b64data=STR2BYTES(cadenaprimitiva)
    b64data=base64.b64decode(b64data)
    archivofuente=archivo64+'D'
    text_file=open(archivofuente,'wb')
    text_file.write(b64data)
    text_file.close()