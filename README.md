Two simple Python functions which transform a file to a base64 code and vice versa.
It is written in Python 3.X


To make the transformation the script must be run in the directory of the files, and then we must pass the complete name of the file to the function f2b64 like this:

f2b64('name of the file')

This makes a new text file with the same name of the original but ended in '.v64' in which there is the base64 code


To make the opposite process we do the same but with the b642f function:

b642f('name of the file with the code')

This makes a new file with the same name of the original (I mean the original with the code) but ended in 'D'. This new file is the original file